---
title: "Avís legal y privacitat"
date: "2020-05-08"
publishDate: "2020-05-08"
draft: false
description: "Informació legal sobre Debut i el maneig de dades en debut.cat"
---

**Debut**       
Lleida 32  
L'Hospitalet de Llobregat  
Barcelona

### Cookies
Utilitzem cookies d'analítica, sobretot per a poder conèixer una mica millor què tal està funcionant la nostra web.  
Pots anul·lar aquesta cookie en qualsevol moment.

### Dades personals
Si t'apuntes al nostre butlletí, guardarem el teu correu electrònic en la nostra base de dades de Mailchimp. També pots modificar les teves dades o donar-te de baixa en qualsevol moment.

Només utilitzem les teves dades per a enviar el butlletí, res més, ni tan sols mirem la llista habitualment.
