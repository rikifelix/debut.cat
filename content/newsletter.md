---
title: "Butlletí setmanal"
date: "2020-05-08"
publishDate: "2020-05-08"
draft: false
description: "El nostre butlletí — Debut"
---


<div id="mc_embed_signup" class="subscribe">
<p>
Tenim un butlletí setmanal per a enviar-te el nostre menú. A vegades, poques, també enviem notícies o curiositats sobre debut.</p>
    <form action="//debut.us13.list-manage.com/subscribe/post?u=85ae56b77267f6c61f49257bd&amp;id=c3ecaf8fec" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form--subscribe" target="_blank" novalidate >
        <div class="mc-field-group">
            <div class="form-items">
                <div class="formGroup">
                    <label for="mce-EMAIL" class="form-label">email</label>
                    <input type="email" value="" name="EMAIL" class="required email form-input" id="mce-EMAIL" onfocus="this.placeholder = ''" onblur="this.placeholder = '...'">
                </div>
                <div class="disclaimer">
                    <input type="checkbox" name="" value="" required class="required" >Acepto la <a href="#">política de privacidad</a>.
                </div>
                <input type="submit" value="Suscribirse" name="subscribe" id="mc-embedded-subscribe" class="button">
            </div>
        <div id="mce-responses">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
        </div>
        </div>
        <div style="position: absolute; left: -5000px;" aria-hidden="true">
            <input type="text" name="b_85ae56b77267f6c61f49257bd_c3ecaf8fec" tabindex="-1" value="">
        </div>
    </form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>
    (function($) {
    window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';
    $.extend( $.validator.messages, {
        required: "Campo obligatorio.",
        remote: "Por favor, rellena este campo.",
        email: "Por favor, escribe una dirección de correo válida.",
        url: "Por favor, escribe una URL válida.",
        date: "Por favor, escribe una fecha válida.",
        dateISO: "Por favor, escribe una fecha (ISO) válida.",
        number: "Por favor, escribe un número válido.",
        digits: "Por favor, escribe sólo dígitos.",
        creditcard: "Por favor, escribe un número de tarjeta válido.",
        equalTo: "Por favor, escribe el mismo valor de nuevo.",
        extension: "Por favor, escribe un valor con una extensión aceptada.",
        maxlength: $.validator.format( "Por favor, no escribas más de {0} caracteres." ),
        minlength: $.validator.format( "Por favor, no escribas menos de {0} caracteres." ),
        rangelength: $.validator.format( "Por favor, escribe un valor entre {0} y {1} caracteres." ),
        range: $.validator.format( "Por favor, escribe un valor entre {0} y {1}." ),
        max: $.validator.format( "Por favor, escribe un valor menor o igual a {0}." ),
        min: $.validator.format( "Por favor, escribe un valor mayor o igual a {0}." ),
        nifES: "Por favor, escribe un NIF válido.",
        nieES: "Por favor, escribe un NIE válido.",
        cifES: "Por favor, escribe un CIF válido."
    } );
    }(jQuery));
    var $mcj = jQuery.noConflict(true);
</script>