---
title: "Contacte"
date: "2022-7-12"
publishDate: "2022-7-12"
draft: false
description: "Contacta amb debut"
---

## Som aquí
Carrer lleida 32, L'Hospitalet de Llobregat  
[↣ Mapa](https://www.google.es/maps/place/Carrer+Lleida,+32,+08901+L'Hospitalet+de+Llobregat,+Barcelona/@41.3625824,2.1008614,18.42z/data=!4m2!3m1!1s0x12a4991f5b5c76a3:0x54c024020321ed42)
## Pots trucar aquí ↣ 93 639 90 88

## Obrim
Els matins, de dimecres a diumenge, de **12:00** a **15:30**  
Las nits del dijous, divendres i dissabtes. de **19:00** a **22:30**
